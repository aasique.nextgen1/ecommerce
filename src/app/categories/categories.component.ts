import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  categories=[
    {'id':1,'name':'Womens','description':'You pretty so wear pretty','image':'../../assets/women.jpg'},
    {'id':2,'name':'Mens','description':'Clothes that Shines you','image':'../../assets/men.jpg'},
    {'id':3,'name':'Kids','description':'A Moments of Spark','image':'../../assets/kids.jpg'},
    {'id':4,'name':'Accessories','description':'For every occasion','image':'../../assets/acc.jpg'}
    
  ]

}
