import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { CategoriesComponent } from './categories/categories.component';
import { HomeComponent } from './home/home.component';
import { SignComponent } from './sign/sign.component';

const routes: Routes = 
[
  {path:'',component:HomeComponent},
  {path:'categories',component:CategoriesComponent},
  {path:'sign',component:SignComponent},
  {path:'about',component:AboutComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
